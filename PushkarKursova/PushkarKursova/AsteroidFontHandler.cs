﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASTEROIDS
{
    class AsteroidsFontHandler
    {
        private frmAsteroids canvas;

        private int m_nTextWidth;
        public int TextWidth { get { return m_nTextWidth; } }
        private int m_nTextHeight;
        public int TextHeight { get { return m_nTextHeight; } }
        private int m_nTextLeft;
        public int TextLeft { get { return m_nTextLeft; } }
        private int m_nTextTop;
        public int TextTop { get { return m_nTextTop; } }

        private int m_nKerning;
        public int Kerning { get { return m_nKerning; } set { m_nKerning = value; } }

        public Point TextPosition
        {
            set { m_nTextLeft = value.X; m_nTextTop = value.Y; }
        }
        private string m_sText;

        private int m_nCharSize;
        public string Text
        {
            get { return m_sText; }
            set
            {
                m_sText = value;
                m_nTextWidth = (int)((m_nCharSize + m_nKerning) * m_sText.Length);
            }
        }

        private float m_fFontSize;
        public float FontSize
        {
            get { return m_fFontSize; }
            set
            {
                m_fFontSize = value;
                m_nCharSize = (int)(0.7f * m_fFontSize);
            }

        }

        public AsteroidsFontHandler(frmAsteroids frm)
        {
            canvas = frm;
        }

        private List<Point> m_secondaryPoints = new List<Point>();
        public void Draw()
        {
            Pen p = new Pen(Color.White);
            var ca = m_sText.ToCharArray();

            for (int x = 0; x < ca.Length; x++)
            {
                char s = ca[x];
                if (s != '.' && s != ' ')
                {
                    canvas.g.DrawLines(p, GetPoints(s, x));
                    if (m_secondaryPoints.Count > 0)
                    {
                        canvas.g.DrawLines(p, m_secondaryPoints.ToArray());
                        m_secondaryPoints.Clear();
                    }

                }
                else if (s == '.')
                {
                    calcOffsetsAndStoreInSecondaryPoints(x);
                    canvas.g.FillRectangle(p.Brush, m_secondaryPoints[0].X, m_secondaryPoints[0].Y, 1, 1);
                    m_secondaryPoints.Clear();
                }
            }
            return;
        }

        private void calcOffsetsAndStoreInSecondaryPoints(int ox)
        {
            int lx = ox * (m_nCharSize + m_nKerning);
            int fs = (int)FontSize;

            m_secondaryPoints.Add(new Point(m_nTextLeft + lx, m_nTextTop + fs));
        }

        private Point[] GetPoints(char cur, int ox)
        {
            List<Point> points = new List<Point>();
            int lx = ox * (m_nCharSize + m_nKerning);

            int fs = (int)FontSize;
            switch (cur)
            {
                case 'A':
                case 'a':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + ((2*fs)/3)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + ((2*fs)/3))
                                    });
                    break;

                case 'B':
                case 'b':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + ((2*fs)/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + ((5*fs)/6)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/6)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (fs/2))

                                    });
                    break;

                case 'C':
                case 'c':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                case 'D':
                case 'd':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop +(fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop +(2*fs/3)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop)
                                    });
                    break;

                case 'E':
                case 'e':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4) , m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });

                    break;
                case 'F':
                case 'f':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4) , m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs)
                                    });
                    break;

                case 'G':
                case 'g':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (2*fs/3)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (2*fs/3))
                                    });
                    break;

                case 'H':
                case 'h':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs/2),
                                        new Point(m_nTextLeft + lx + m_nCharSize , m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize , m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize , m_nTextTop + fs)
                                    });
                    break;

                case 'I':
                case 'i':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop +fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop +fs)
                                    });
                    break;


                case 'J':
                case 'j':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (2*fs/3))
                                    });
                    break;

                case 'K':
                case 'k':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                case 'L':
                case 'l':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize , m_nTextTop + fs)
                                    });
                    break;

                case 'M':
                case 'm':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;


                case 'N':
                case 'n':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs)
                                    });
                    break;

                case 'P':
                case 'p':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2))
                                    });
                    break;
                case 'Q':
                case 'q':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (2*fs/3)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (5*fs/6)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (2*fs/3)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (5*fs/6))
                                    });
                    break;
                case 'R':
                case 'r':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                case 'S':
                case 's':
                case '5':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                    });
                    break;
                case 'T':
                case 't':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs)
                                    });
                    break;

                case 'U':
                case 'u':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop)
                                    });
                    break;

                case 'V':
                case 'v':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop)
                                    });
                    break;

                case 'W':
                case 'w':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (2*fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop)
                                    });
                    break;

                case 'X':
                case 'x':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs)
                                    });
                    break;

                case 'Y':
                case 'y':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + (fs/3)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs)
                                    });
                    break;

                case 'Z':
                case 'z':
                    points.AddRange(new[]
                                        {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;
                case '0':
                case 'O':
                case 'o':
                    points.AddRange(new[]
                                    {   new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop)
                                    });
                    break;

                case '1':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/2), m_nTextTop + fs),
                                    });
                    break;

                case '2':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                    });
                    break;

                case '3':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                    });
                    break;

                case '4':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                case '6':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2))
                                    });
                    break;

                case '7':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                case '8':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                    });
                    break;

                case '9':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/2)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/2))
                                    });
                    break;

                case '©':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx + (m_nCharSize/4), m_nTextTop),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (fs/6)),
                                        new Point(m_nTextLeft + lx, m_nTextTop + (5*fs/6)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/4), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (5*fs/6)),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + (fs/6)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/4), m_nTextTop)
                                    });

                    m_secondaryPoints.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (fs/6)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/4), m_nTextTop + (fs/6)),
                                        new Point(m_nTextLeft + lx + (m_nCharSize/4), m_nTextTop + (5*fs/6)),
                                        new Point(m_nTextLeft + lx + (3*m_nCharSize/4), m_nTextTop + (5*fs/6))
                                    });
                    break;

                case '_':
                    points.AddRange(new[]
                                    {
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs)
                                    });
                    break;

                default:
                    points.AddRange(new[]
                                    {   new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop),
                                        new Point(m_nTextLeft + lx+m_nCharSize, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx, m_nTextTop + fs),
                                        new Point(m_nTextLeft + lx + m_nCharSize, m_nTextTop)
                                    });
                    break;

            }
            return points.ToArray();
        }
    }
}


namespace ASTEROIDS
{
    public class HighScores
    {
        private int MAXHIGHSCORES = 10;
        private List<HighScore> m_lstHighScores = new List<HighScore>();
        private String HighScoresPath = Directory.GetCurrentDirectory() + "\\HighScores.DAT";

        public HighScores()
        {
            Load();
        }

        public List<HighScore> list
        {
            get { return m_lstHighScores; }
        }

        public void Save()
        {
            List<string> lines = new List<string>();
            foreach (HighScore hs in m_lstHighScores)
            {
                lines.Add(String.Format("{0}{1}", hs.Initials, hs.Score));
            }
            File.WriteAllLines(@HighScoresPath, lines.ToArray(), Encoding.UTF8);

            return;
        }

        public bool Load()
        {
            m_lstHighScores.Clear();
            if (File.Exists(HighScoresPath))
            {
                string[] lines = System.IO.File.ReadAllLines(@HighScoresPath);

                for (int nIndex = 0; nIndex < lines.Length; nIndex++)
                {
                    if (nIndex >= MAXHIGHSCORES)
                        break;

                    HighScore hs = new HighScore();
                    hs.Initials = lines[nIndex].Substring(0, 3);
                    hs.Score = System.Convert.ToInt32(lines[nIndex].Substring(3, lines[nIndex].Length - 3));
                    m_lstHighScores.Add(hs);
                }
            }

            return true;
        }

        public bool isHighScore(int score)
        {
            bool bIshighScore = false;
            if (m_lstHighScores.Count < MAXHIGHSCORES) bIshighScore = true;
            else
            {
                for (int nIndex = 0; nIndex < m_lstHighScores.Count; nIndex++)
                {
                    if (m_lstHighScores[nIndex].Score < score)
                    {
                        bIshighScore = true;
                        break;
                    }
                }
            }


            return bIshighScore;
        }

        public void AddScore(HighScore hs)
        {
            bool bIshighScore = false;
            int nInsertAt = 0;
            for (int nIndex = 0; nIndex < m_lstHighScores.Count; nIndex++)
            {
                if (m_lstHighScores[nIndex].Score < hs.Score)
                {
                    bIshighScore = true;
                    nInsertAt = nIndex;
                    break;
                }
            }

            if (m_lstHighScores.Count < MAXHIGHSCORES)
            {
                m_lstHighScores.Add(hs);
            }
            else if (bIshighScore == true)
            {
                m_lstHighScores.Insert(nInsertAt, hs);
                m_lstHighScores.RemoveAt(m_lstHighScores.Count - 1);
            }
            Save(); Load();
        }

        public int GetHighScore()
        {
            int nScore = 0;
            if (m_lstHighScores.Count > 0)
                nScore = m_lstHighScores.OrderByDescending(hs => hs.Score).ToList()[0].Score;

            return nScore;
        }

    }

    public class HighScore
    {
        private int m_nCurInitialIndex = 0;

        private string m_sInitials = "A__";
        public string Initials
        {
            get { return m_sInitials; }
            set { m_sInitials = value; }
        }
        private int m_nScore;
        public int Score
        {
            get { return m_nScore; }
            set { m_nScore = value; }
        }

        public HighScore()
        {
        }
    }

    public class EnterHighScoreScreen
    {
        public frmAsteroids canvas;
        private int m_CurIndex = 0;
        public bool m_bDisplay = false;
        public HighScore newHighScore = new HighScore();

        public bool IsDone
        {
            get { return m_CurIndex > 2; }
        }

        public EnterHighScoreScreen(frmAsteroids frm)
        {
            canvas = frm;
            newHighScore.Initials = "A__";
        }

        public void Draw()
        {
            if (m_bDisplay)
            {
                // ASTEROIDS
                AsteroidsFontHandler afh = new AsteroidsFontHandler(canvas);
                System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);

                int topPosOfBottomRowOfHelpText = 485;

                // Since the bottom of this text is perfectly centered, and all other text is centered accordingly,
                // first I find the left for that text and then use that for the left on previous message text
                // SCOREBOARD LEFT!
                afh.FontSize = 32.0f; afh.Kerning = 10;
                afh.Text = "PUSH HYPERSPACE WHEN LETTER IS CORRECT";
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), topPosOfBottomRowOfHelpText);
                afh.Draw();

                int left = afh.TextLeft;
                afh.Text = "PUSH ROTATE TO SELECT LETTER";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

                afh.Text = "PLEASE ENTER YOUR INITIALS";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

                afh.Text = "YOUR SCORE IS ONE OF THE TEN BEST";
                afh.TextPosition = new Point(left, topPosOfBottomRowOfHelpText -= 50);
                afh.Draw();

                afh.FontSize = 50.0f; afh.Kerning = 20;
                afh.Text = newHighScore.Initials;
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2) - 28, canvas.Height - 215);
                afh.Draw();
            }
        }

        public bool ProcessKey(Keys key)
        {
            bool bProcessed = false;
            if (!IsDone)
            {
                char cur = newHighScore.Initials[m_CurIndex];
                char[] allInitials = newHighScore.Initials.ToCharArray();
                string allChars = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                int indexofCurChar = allChars.IndexOf(cur);

                switch (key)
                {
                    case Keys.Left:
                        if (indexofCurChar == 0)
                            indexofCurChar = allChars.Length;
                        cur = allChars.ToCharArray()[indexofCurChar - 1];
                        allInitials[m_CurIndex] = cur;
                        newHighScore.Initials = string.Join("", allInitials);
                        bProcessed = true;
                        break;
                    case Keys.Right:
                        if (indexofCurChar == allChars.Length - 1)
                            indexofCurChar = -1;
                        cur = allChars.ToCharArray()[indexofCurChar + 1];
                        allInitials[m_CurIndex] = cur;
                        newHighScore.Initials = string.Join("", allInitials);
                        bProcessed = true;
                        break;
                    case Keys.Down:
                        m_CurIndex += 1;
                        bProcessed = true;
                        break;
                }
            }

            return bProcessed;
        }
    }
}


namespace ASTEROIDS
{
    public class ScoreBoard
    {
        public int SCORE = 0;
        public int HIGHSCORE = 1000000;
        private int m_nShipsLeft = 0;
        private List<Ship> m_ShipsLeft = new List<Ship>();
        public int SHIPSLEFT
        {
            get { return m_nShipsLeft; }
            set
            {
                m_nShipsLeft = value;
                if (m_ShipsLeft.Count != m_nShipsLeft)
                {
                    m_ShipsLeft.Clear();
                    for (int x = 0; x < m_nShipsLeft; x++)
                    {
                        Ship cur = new Ship(canvas);
                        cur.position = new Point(445 + (x * 23), 168);
                        m_ShipsLeft.Add(cur);
                    }
                }
            }
        }

        public int CURLEVEL = 0;
        public string STARTMESSAGE = "PUSH START";
        public string GAMEOVERMESSAGE = "GAME OVER";
        public string PLAYERMESSAGE = "PLAYER 1";

        public long blinkTicks;
        public bool m_bIsPlaying = false;
        private bool m_bBlinkOn = true;

        private const double DELAYBETWEENHIGHSCOREDISPLAY = 16.0f;
        private bool m_bIsHighScoreScreenVisible = true;
        public void displayHighScoreScreen()
        {
            m_bIsHighScoreScreenVisible = true;
            m_lNextToggle = System.DateTime.Now.AddTicks((long)(DELAYBETWEENHIGHSCOREDISPLAY * (double)frmAsteroids.TICKSPERSECOND)).Ticks;
        }
        private long m_lNextToggle;

        private frmAsteroids canvas;

        private HighScores m_highScores = new HighScores();
        public HighScores highScores
        {
            get { return m_highScores; }
        }

        public ScoreBoard(frmAsteroids frm)
        {
            canvas = frm;
            m_lNextToggle = System.DateTime.Now.AddTicks((long)(DELAYBETWEENHIGHSCOREDISPLAY * (double)frmAsteroids.TICKSPERSECOND)).Ticks;
            blinkTicks = System.DateTime.Now.AddTicks(6000000).Ticks;
        }

        private Point m_retainStartMsgLeft = new Point();

        public void Draw()
        {
            AsteroidsFontHandler afh = new AsteroidsFontHandler(canvas);
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);

            afh.FontSize = 32.0f; afh.Kerning = 7;
            afh.Text = SCORE.ToString("D2");
            afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2) - 410 - (int)afh.TextWidth, 60);
            afh.Draw();

            afh.FontSize = 18.0f;
            afh.Kerning = 3;
            afh.Text = m_highScores.GetHighScore().ToString();
            afh.TextPosition = new Point((int)((canvas.Width - afh.TextWidth) / 2), 70);
            afh.Draw();

            afh.FontSize = 32.0f; afh.Kerning = 10;
            afh.Text = SCORE.ToString("D2");


            if (System.DateTime.Now.Ticks > blinkTicks)
            {
                blinkTicks = System.DateTime.Now.AddTicks(4750000).Ticks;
                m_bBlinkOn = !m_bBlinkOn;
            }

            if (m_bBlinkOn && !m_bIsPlaying)
            {
                afh.Kerning = 10;
                afh.Text = STARTMESSAGE;
                m_retainStartMsgLeft = new Point(((canvas.Width - afh.TextWidth) / 2), 175);
                afh.TextPosition = m_retainStartMsgLeft;

                afh.Draw();
            }
            if (!m_bIsPlaying)
            {
                if (m_lNextToggle <= System.DateTime.Now.Ticks)
                {
                    m_bIsHighScoreScreenVisible = !m_bIsHighScoreScreenVisible;

                    m_lNextToggle = System.DateTime.Now.AddTicks((long)(DELAYBETWEENHIGHSCOREDISPLAY *
                                                                       (double)frmAsteroids.TICKSPERSECOND)).Ticks;
                }

                if (m_bIsHighScoreScreenVisible)
                {
                    afh.Kerning = 10;
                    afh.Text = "";
                    afh.TextPosition = new Point(m_retainStartMsgLeft.X, m_retainStartMsgLeft.Y + 90);
                    afh.Draw();

                    int nX = 0;
                    foreach (HighScore hs in m_highScores.list.OrderByDescending(hs => hs.Score))
                    {
                        String replaceUnderscores = hs.Initials.Replace('_', ' ');
                        afh.Text = String.Format("{0}{1}.{2,5} {3}",
                                            ((nX + 1).ToString().Length == 1) ? " " : "",
                                            nX + 1,
                                            hs.Score,
                                            replaceUnderscores);
                        afh.TextPosition = new Point(m_retainStartMsgLeft.X - 25, m_retainStartMsgLeft.Y + 175 + (nX * 40));
                        afh.Draw();
                        nX += 1;
                    }
                }

            }
            else if (m_bIsPlaying)
            {
                afh.Text = PLAYERMESSAGE;
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), 225);
                afh.Draw();

                foreach (Ship s in m_ShipsLeft)
                    s.Draw();
            }

            if (canvas.m_bGameOver && canvas.m_EnterHighScoreScreen.m_bDisplay != true)
            {
                afh.Text = GAMEOVERMESSAGE;
                afh.TextPosition = new Point(((canvas.Width - afh.TextWidth) / 2), (canvas.Height / 2) - 100);
                afh.Draw();
            }
        }

    }
}
