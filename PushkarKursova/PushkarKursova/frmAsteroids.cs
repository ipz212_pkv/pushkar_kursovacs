using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace ASTEROIDS
{
    public partial class frmAsteroids : Form
    {
        private int m_lX = 0;
        private int m_lY = 0;

        private const int DLEN = 2;
        private const long DELAYBETWEENLEVELS = 25000000;
        private const float PLAYERRESURRECTIONDELAY = 3.0f;
        public const long TICKSPERSECOND = 10000000;
        public Random randomizer = new Random(DateTime.Now.TimeOfDay.Milliseconds);


        private bool bCurLevelTransition = false;

        private List<Asteroid> m_asteroids = new List<Asteroid>();
        private Ship m_player;
        private Projectiles m_projectiles; private bool m_bIsSpacePressed = false;
        private Projectiles m_ufoprojectiles;
        private ScoreBoard m_mainScoreBoard;
        public EnterHighScoreScreen m_EnterHighScoreScreen;
        public bool? m_bDisplayEnterHighScoreScreen = null;
        private const double TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN = 3.0f;
        private const double TIMEOUTFORHIGHSCOREDISPLAYSCREEN = 30.0f;
        private long ticksHighScoreDisplayScreenTimesOut;

        private UFO m_ufo;

        public Graphics g;
        public float ASPECTRATIO;
        public string DEBUGOUTPUT = "";
        private long m_nTicksSinceStart;

        private long[] m_nPlayerDestroyedSFTicks = new long[2];
        private long m_nUFOFiringSFTicks;
        private long[] m_nPlayerInHyperSpaceSFTicks = new long[2];

        private int m_numDemoScreensteroids = 10;

        public bool m_bGameOver = false;
        public long m_nGameOverResetAtTicks = 0;

        public long m_NextRandomUFOEncounter;


        private long m_tckNextBGSound;
        private bool m_bBGSoundLow = false;
        private bool m_bPlayerIsThrusting = false;
        private double m_AmbientAccelerator = 1.0;

        public frmAsteroids()
        {
            InitializeComponent();

            System.Windows.Forms.Cursor.Hide();
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Top = 0;
            this.Left = 0;

            ASPECTRATIO = (float)Screen.PrimaryScreen.Bounds.Height / (float)Screen.PrimaryScreen.Bounds.Width;

            timer1.Interval = (int)(1000.0f / 60.0f);

            m_player = new Ship(this);
            m_mainScoreBoard = new ScoreBoard(this);
            m_projectiles = new Projectiles(this);
            m_ufoprojectiles = new Projectiles(this);
            m_ufo = new UFO(this);
            m_EnterHighScoreScreen = new EnterHighScoreScreen(this);
            m_ufo.position = new Point(250, 250);


            for (int x = 0; x < m_numDemoScreensteroids; x++)
            {
                m_asteroids.Add(new Asteroid(this, (Asteroid.SIZEOFASTEROID)randomizer.Next(0, 3)));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            long lCurTicks = System.DateTime.Now.Ticks;
            if (m_mainScoreBoard.m_bIsPlaying && !m_bGameOver)
            {
                if (m_asteroids.Count > 0 && !m_player.m_bIsActive)
                {
                    m_asteroids.Clear();
                    m_mainScoreBoard.SCORE = 0;
                }
                else if ((lCurTicks - m_nTicksSinceStart) > 20000000 && !m_player.m_bIsActive)
                {
                    m_player.m_bIsActive = true;
                    m_mainScoreBoard.PLAYERMESSAGE = "";

                    m_mainScoreBoard.CURLEVEL = 1;
                    m_AmbientAccelerator = 1.0;
                    m_NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 15)).Ticks;
                    loadAsteroidsForThisLevel();
                }
                else if (m_asteroids.Count == 0 && m_player.m_bIsActive && !bCurLevelTransition)
                {
                    m_nTicksSinceStart = System.DateTime.Now.Ticks;

                    m_mainScoreBoard.CURLEVEL += 1;
                    m_AmbientAccelerator = 1.0;

                    switch (m_mainScoreBoard.CURLEVEL)
                    {
                        case 2:
                            m_NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(10, 30)).Ticks;
                            break;
                        default:
                            m_NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(10, 30)).Ticks;
                            break;
                    }
                    bCurLevelTransition = true;
                }
                else if (bCurLevelTransition && ((lCurTicks - m_nTicksSinceStart) > DELAYBETWEENLEVELS))
                {
                    bCurLevelTransition = false;
                    loadAsteroidsForThisLevel();
                }

                if (m_player.bIsHyperSpace)
                {
                    m_nPlayerInHyperSpaceSFTicks[1] = DateTime.Now.Ticks;
                    if (m_nPlayerInHyperSpaceSFTicks[0] < m_nPlayerInHyperSpaceSFTicks[1])
                    {
                        m_player.velocity = 0.0f;
                        m_player.position.X = randomizer.Next(200, Screen.PrimaryScreen.Bounds.Width - 200);
                        m_player.position.Y = randomizer.Next(250, Screen.PrimaryScreen.Bounds.Height - 250);
                        m_player.bIsHyperSpace = false;
                    }
                }

                if (!m_player.bPlayerIsDestroyed)
                {
                    foreach (Asteroid a in m_asteroids)
                    {
                        if (m_player.doesObjectCollide(a.Position, a.myRadius))
                        {
                            if (!m_player.bPlayerIsDestroyed)
                            {
                                a.bPlayerCollided = true;
                                initiatePlayerDestroyedSequence();
                            }
                            break;
                        }
                    }
                }
                if (m_player.bPlayerIsDestroyed)
                {
                    m_nPlayerDestroyedSFTicks[1] = DateTime.Now.Ticks;
                    if ((m_nPlayerDestroyedSFTicks[1] - m_nPlayerDestroyedSFTicks[0]) > (PLAYERRESURRECTIONDELAY * TICKSPERSECOND))
                    {
                        m_player.resetToCenter();
                        foreach (Asteroid a in m_asteroids)
                        {
                            if (a.doesPointCollide(m_player.position))
                                goto fuggedAboutIt;
                        }

                        m_player.bPlayerIsDestroyed = false;
                        m_player.velocity = 0.0f;
                    }
                }

            fuggedAboutIt:
                if (m_ufo.IsActive && !m_ufo.bUFOIsDestroyed)
                {

                    double firingRate = (m_ufo.m_ufoType == UFO.UFOSIZE.LARGE) ? 0.9 : 0.7;

                    if ((((double)System.DateTime.Now.Ticks - (double)m_nUFOFiringSFTicks) / (double)TICKSPERSECOND) >= firingRate)
                    {
                        m_nUFOFiringSFTicks = System.DateTime.Now.Ticks;

                        int rVal = (m_ufo.m_ufoType == UFO.UFOSIZE.LARGE) ? randomizer.Next(1, 5) : randomizer.Next(1, 4);
                        bool bIsRandom = (rVal != 1);

                        if (bIsRandom)
                            m_ufoprojectiles.RandomFire(m_ufo.position);
                        else
                        {
                            float angle = (float)Math.Atan2(m_player.position.Y - m_ufo.position.Y,
                                                             m_player.position.X - m_ufo.position.X);

                            angle = angle + ((float)Math.PI / 2.0f);

                            m_ufoprojectiles.NotSoRandomFire(m_ufo.position, angle);
                        }
                    }

                    if (m_player.doesObjectCollide(m_ufo.position, UFO.RADIUS))
                    {
                        initiatePlayerDestroyedSequence();
                        m_ufo.triggerCollisionSequence();

                        if (m_ufo.m_ufoType == UFO.UFOSIZE.SMALL)
                            m_mainScoreBoard.SCORE += 1000;
                        else
                            m_mainScoreBoard.SCORE += 200;
                    }
                    else
                    {
                        foreach (Asteroid a in m_asteroids)
                        {
                            if (m_ufo.doesObjectCollide(a.Position, a.myRadius))
                            {
                                m_ufo.bUFOIsDestroyed = true;
                                a.bDestroyed = true;
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    for (int nX = 0; nX < 2; nX++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, a.Position);
                                        newOne.m_fMoveAngle = a.m_fMoveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        m_asteroids.Add(newOne);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                if (m_NextRandomUFOEncounter < System.DateTime.Now.Ticks && !m_ufo.IsActive && m_player.m_bIsActive)
                {
                    m_nUFOFiringSFTicks = System.DateTime.Now.Ticks;
                    if ((System.DateTime.Now.Ticks - m_nTicksSinceStart) < (10 * TICKSPERSECOND))
                        m_AmbientAccelerator = 1.0;
                    else if ((System.DateTime.Now.Ticks - m_nTicksSinceStart) < (20 * TICKSPERSECOND))
                        m_AmbientAccelerator = 0.8;
                    else if ((System.DateTime.Now.Ticks - m_nTicksSinceStart) < (30 * TICKSPERSECOND))
                        m_AmbientAccelerator = 0.6;
                    else
                        m_AmbientAccelerator = 0.4;

                    m_ufo.spawnUFO(m_mainScoreBoard.CURLEVEL, System.DateTime.Now.Ticks - m_nTicksSinceStart);
                    m_NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 20)).Ticks;
                }
                else if (m_ufo.IsActive && m_player.m_bIsActive)
                {
                    m_NextRandomUFOEncounter = System.DateTime.Now.AddTicks(TICKSPERSECOND * randomizer.Next(5, 15)).Ticks;
                }
            }
            else if (m_bGameOver)
            {
                if (m_bDisplayEnterHighScoreScreen == null)
                {
                    if (m_mainScoreBoard.highScores.isHighScore(m_mainScoreBoard.SCORE))
                    {
                        m_bDisplayEnterHighScoreScreen = true;
                        m_EnterHighScoreScreen = new EnterHighScoreScreen(this);
                        m_EnterHighScoreScreen.newHighScore.Score = m_mainScoreBoard.SCORE;
                        ticksHighScoreDisplayScreenTimesOut = System.DateTime.Now.AddTicks(
                            (long)((TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN + TIMEOUTFORHIGHSCOREDISPLAYSCREEN) * (double)TICKSPERSECOND)).Ticks;
                    }
                    else if ((m_bDisplayEnterHighScoreScreen == true) && (ticksHighScoreDisplayScreenTimesOut > System.DateTime.Now.Ticks))
                    {
                    }
                    else
                    {
                        m_bDisplayEnterHighScoreScreen = false;
                    }
                }
                if (m_bDisplayEnterHighScoreScreen == true)
                {
                    long curTicks = (ticksHighScoreDisplayScreenTimesOut - DateTime.Now.Ticks) / TICKSPERSECOND;
                    if (curTicks > TIMEOUTBEFOREDISPLAYINGHIGHSCORESCREEN &&
                       curTicks < TIMEOUTFORHIGHSCOREDISPLAYSCREEN)
                    {
                        m_bGameOver = true;
                        m_player.m_bIsActive = false;
                        m_EnterHighScoreScreen.m_bDisplay = true;
                        if (m_EnterHighScoreScreen.IsDone)
                        {
                            m_EnterHighScoreScreen.m_bDisplay = false;
                            m_mainScoreBoard.highScores.AddScore(m_EnterHighScoreScreen.newHighScore);
                            m_mainScoreBoard.displayHighScoreScreen();
                            m_bDisplayEnterHighScoreScreen = null;
                            m_mainScoreBoard.m_bIsPlaying = false;
                            m_player.m_bIsActive = false;
                            m_bGameOver = false;
                        }
                    }
                    else if (curTicks <= 0)
                    {
                        m_bDisplayEnterHighScoreScreen = null;
                        m_mainScoreBoard.m_bIsPlaying = false;
                        m_player.m_bIsActive = false;
                        m_bGameOver = false;
                        m_EnterHighScoreScreen.m_bDisplay = false;
                    }
                }
                else if (m_bDisplayEnterHighScoreScreen == false || m_bDisplayEnterHighScoreScreen == null)
                {
                    if (m_nGameOverResetAtTicks < DateTime.Now.Ticks)
                    {
                        m_bDisplayEnterHighScoreScreen = null;
                        m_mainScoreBoard.m_bIsPlaying = false;
                        m_player.m_bIsActive = false;
                        m_bGameOver = false;
                    }
                }
            }

            if (m_mainScoreBoard.m_bIsPlaying && !m_bGameOver)
            {
                if (m_tckNextBGSound < System.DateTime.Now.Ticks)
                {
                    m_tckNextBGSound = System.DateTime.Now.AddTicks((long)((double)TICKSPERSECOND * m_AmbientAccelerator)).Ticks;
                    m_bBGSoundLow = !m_bBGSoundLow;
                }
            }
            else
                m_bPlayerIsThrusting = false;

            Invalidate();
        }

        private void loadAsteroidsForThisLevel()
        {
            int nL = 0, nM = 0, nS = 0;

            switch (m_mainScoreBoard.CURLEVEL)
            {
                case 1:
                    nL = 4;
                    break;
                case 2:
                    nL = 6;
                    break;
                case 3:
                    nL = 8;
                    break;
                case 4:
                    nL = 9;
                    break;
                case 5:
                    nL = 10;
                    break;

                default:
                    nL = 11;
                    break;
            }
            int nCur;
            for (nCur = 0; nCur < nL; nCur++)
            {
            tryAgain:
                Asteroid newOne = new Asteroid(this, Asteroid.SIZEOFASTEROID.LARGE);
                if (m_player.doesObjectCollide(newOne.Position, (int)newOne.myRadius))
                    goto tryAgain;

                m_asteroids.Add(newOne);
            }

        }

        private void frmAsteroids_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    if (!m_mainScoreBoard.m_bIsPlaying)
                    {
                        m_mainScoreBoard.m_bIsPlaying = true;
                        m_mainScoreBoard.SHIPSLEFT = 3;

                        m_nTicksSinceStart = System.DateTime.Now.Ticks;
                    }
                    break;
                case Keys.Up:
                    if (m_player.m_bIsActive)
                    {
                        m_player.Accelerate(e.KeyCode, true);
                        m_bPlayerIsThrusting = true;
                    }
                    break;
                case Keys.Left:
                case Keys.Right:
                    if (m_player.m_bIsActive)
                        m_player.Rotate(e.KeyCode, true);
                    else if (m_bDisplayEnterHighScoreScreen == true)
                        m_EnterHighScoreScreen.ProcessKey(e.KeyCode);
                    break;
                case Keys.Down:
                    if (m_player.m_bIsActive)
                    {
                        m_nPlayerInHyperSpaceSFTicks[0] = System.DateTime.Now.Ticks + (randomizer.Next(5, 25) * 1000000);
                        m_player.bIsHyperSpace = true;
                    }
                    else if (m_bDisplayEnterHighScoreScreen == true)
                        m_EnterHighScoreScreen.ProcessKey(e.KeyCode);

                    break;
                case Keys.Space:
                    if (m_player.m_bIsActive)
                    {
                        if (!m_bIsSpacePressed)
                        {
                            m_projectiles.Fire(m_player, m_player.position, m_player.rotationAngle, m_player.velocity);
                            m_bIsSpacePressed = true;
                        }
                    }
                    break;
            }
        }

        private void frmAsteroids_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (m_player.m_bIsActive)
                    {
                        m_player.Accelerate(e.KeyCode, false);
                        m_bPlayerIsThrusting = false;
                    }

                    break;
                case Keys.Left:
                case Keys.Right:
                    if (m_player.m_bIsActive)
                        m_player.Rotate(e.KeyCode, false);
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
                case Keys.Space:
                    if (m_bIsSpacePressed)
                        m_bIsSpacePressed = false;
                    break;
                default:
                    break;
            }
        }

        private void frmAsteroids_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;

            List<Projectile> list = m_projectiles.list;
            List<Projectile> listUFOProjectiles = m_ufoprojectiles.list;

            for (int aCur = m_asteroids.Count - 1; aCur >= 0; aCur--)
            {
                Asteroid a = m_asteroids[aCur];
                if (!a.bDestroyed)
                {
                    if (list.Count > 0)
                    {
                        for (int nCur = list.Count - 1; nCur >= 0; nCur--)
                        {
                            if (m_ufo.IsActive && !m_ufo.bUFOIsDestroyed)
                            {
                                if (m_ufo.doesObjectCollide(list[nCur].position, UFO.RADIUS))
                                {
                                    if (m_ufo.m_ufoType == UFO.UFOSIZE.SMALL)
                                        m_mainScoreBoard.SCORE += 1000;
                                    else
                                        m_mainScoreBoard.SCORE += 200;

                                    m_ufo.bUFOIsDestroyed = true;
                                }
                            }

                            if (a.doesPointCollide(list[nCur].position) && !m_player.bPlayerIsDestroyed)
                            {
                                a.bDestroyed = true;
                                m_mainScoreBoard.SCORE += a.ASTEROIDPOINTVALUES[(int)a.mySize];
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    for (int nX = 0; nX < 2; nX++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, list[nCur].position);
                                        newOne.m_fMoveAngle = a.m_fMoveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        m_asteroids.Add(newOne);
                                    }
                                }

                                list.RemoveAt(nCur);
                            }
                        }
                    }
                    if (listUFOProjectiles.Count > 0)
                    {
                        for (int nCur = listUFOProjectiles.Count - 1; nCur >= 0; nCur--)
                        {
                            if (m_player.m_bIsActive && !m_player.bPlayerIsDestroyed)
                            {
                                if (m_player.doesObjectCollide(listUFOProjectiles[nCur].position, 10))
                                {
                                    if (!m_player.bPlayerIsDestroyed)
                                    {
                                        initiatePlayerDestroyedSequence();
                                    }
                                    listUFOProjectiles.RemoveAt(nCur);
                                    break;
                                }
                            }

                            if (a.doesPointCollide(listUFOProjectiles[nCur].position))
                            {
                                a.bDestroyed = true;
                                if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                                {
                                    for (int nX = 0; nX < 2; nX++)
                                    {
                                        Asteroid newOne = new Asteroid(this, a.mySize - 1, listUFOProjectiles[nCur].position);
                                        newOne.m_fMoveAngle = a.m_fMoveAngle + randomizer.Next(-25, 25);
                                        newOne.newPseudoRandomVelocity(a);
                                        m_asteroids.Add(newOne);
                                    }
                                }
                                listUFOProjectiles.RemoveAt(nCur);
                            }
                        }
                    }

                    if (a.bPlayerCollided)
                    {
                        a.bDestroyed = true;
                        a.collisionLocation = m_player.position;
                        m_mainScoreBoard.SCORE += a.ASTEROIDPOINTVALUES[(int)a.mySize];
                        if (a.mySize != Asteroid.SIZEOFASTEROID.SMALL)
                        {
                            for (int nX = 0; nX < 2; nX++)
                            {
                                Asteroid newOne = new Asteroid(this, a.mySize - 1, m_player.position);
                                newOne.m_fMoveAngle = a.m_fMoveAngle + randomizer.Next(-25, 25);
                                newOne.newPseudoRandomVelocity(a);
                                m_asteroids.Add(newOne);
                            }
                        }
                    }

                    if (!m_EnterHighScoreScreen.m_bDisplay)
                    {
                        a.Draw();
                        a.Move();
                    }
                }
                else if (a.destructionAnimation.radius < a.MAXANIMRADIUS)
                {
                    if (!m_EnterHighScoreScreen.m_bDisplay)
                    {
                        a.destructionAnimation.radius += 2.25f;
                        a.Draw();
                    }
                }
                else
                    m_asteroids.RemoveAt(aCur);

            }
            if (m_player.m_bIsActive)
            {
                m_player.Draw();
                m_projectiles.Draw();
            }

            if (!m_EnterHighScoreScreen.m_bDisplay)
            {
                if (m_ufo.IsActive)
                    m_ufoprojectiles.Draw();

                m_ufo.Draw();
            }

            m_mainScoreBoard.Draw();
            m_EnterHighScoreScreen.Draw();

        }

        private void initiatePlayerDestroyedSequence()
        {
            m_player.bPlayerIsDestroyed = true;
            m_mainScoreBoard.SHIPSLEFT -= 1;
            if (m_mainScoreBoard.SHIPSLEFT == 0)
            {
                m_bGameOver = true;
                m_nGameOverResetAtTicks = DateTime.Now.Ticks + (4 * TICKSPERSECOND);
            }

            m_nPlayerDestroyedSFTicks[0] = DateTime.Now.Ticks;
        }

        public void onUFOExit()
        {
        }

        private void frmAsteroids_Load(object sender, EventArgs e)
        {
            DoubleBuffered = true;
        }
    }
}
